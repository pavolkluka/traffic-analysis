#!/bin/bash

if [ -z "$1" ]
then
      echo "Please specify eml file as argument for this script"
	  exit 1
else
      FILE_EML="$1"
fi

# GET RECEIVED FROM SERVER IP, SOURCE PORT, MAIL ADDRESS
VAR_TEMP="$( awk -F '(' '/Received: from \[/ {print $2}' $FILE_EML )"
SOURCE_IP="$( echo $VAR_TEMP | grep -oP '(?<=\[)[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(?=\:)' )"
SOURCE_PORT="$( echo $VAR_TEMP | grep -oP '(?<=\:)[0-9]{1,5}(?=\])' )"
SOURCE_MAIL="$( echo $VAR_TEMP | grep -oP '(?<=\=).*?(?=\))' )"

# GET OTHER INFORMATION
MAIL_DATE="$( grep -oP '(?<=^Date\:\s).*?(?=$)' $FILE_EML )"
MAIL_FROM="$( grep -oP '(?<=^From\:\s).*?(?=$)' $FILE_EML )"
MAIL_TO="$( grep -oP '(?<=^To\:\s).*?(?=$)' $FILE_EML )"
MAIL_SUBJECT="$( grep -oP '(?<=^Subject\:\s).*?(?=$)' $FILE_EML )"

ATTACHMENT_TYPE="$( grep -oP '(?<=^Content\-Type\:\s).*?(?=\;)' $FILE_EML | tail -1 )"
ATTACHMENT_NAME="$( awk -F ':' '/^Content-Disposition/ {print $2}' $FILE_EML | grep -oP '(?<=filename\=\").*?(?=\")' )"

echo -e "Source IP:\t$SOURCE_IP"
echo -e "Source Port:\t$SOURCE_PORT"
echo -e "Source Mail:\t$SOURCE_MAIL"

echo -e "Date:\t$MAIL_DATE"
echo -e "From:\t$MAIL_FROM"
echo -e "To:\t$MAIL_TO"
echo -e "Subject:\t$MAIL_SUBJECT"
echo -e "Attachment type:\t$ATTACHMENT_TYPE"
echo -e "Attachment name:\t$ATTACHMENT_NAME"

