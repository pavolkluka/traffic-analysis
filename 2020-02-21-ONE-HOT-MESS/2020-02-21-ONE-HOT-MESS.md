# All aboard the hot mess express!

Link: [2020-02-21 -- Traffic analysis exercise - All aboard the hot mess express!](http://malware-traffic-analysis.net/2020/02/21/index.html)

## WORKING FILES

PCAP: [2020-02-21-traffic-analysis-exercise.pcap.zip](http://malware-traffic-analysis.net/2020/02/21/2020-02-21-traffic-analysis-exercise.pcap.zip)

ALERTS: [2020-02-21-traffic-analysis-exercise-alerts.zip](http://malware-traffic-analysis.net/2020/02/21/2020-02-21-traffic-analysis-exercise-alerts.zip)

MALWARE AND ARTIFACTS: [2020-02-21-traffic-analysis-exercise-malware-and-artifacts.zip](http://malware-traffic-analysis.net/2020/02/21/2020-02-21-traffic-analysis-exercise-malware-and-artifacts.zip)

## INFORMATION

- LAN segment range:  172.17.8.0/24 (172.17.8.0 through 172.17.8.255)
- Domain:  one-hot-mess.com
- Domain controller:  172.17.8.8 - One-Hot-Mess-DC
- LAN segment gateway:  172.17.8.1
- LAN segment broadcast address:  172.17.8.255

## PCAP with Alert from Suricata

### Information about pcap file

You can use Wireshark (Menu bar -> Statistics -> **Capture File Properties**) or command line tool **capinfos**. I usualy use capinfos. Output is under name 00-pcap-capinfos.txt in evidence folder.

Output:
```bash
File name:           2020-02-21-traffic-analysis-exercise.pcap
File type:           Wireshark/tcpdump/... - pcap
File encapsulation:  Ethernet
File timestamp precision:  microseconds (6)
Packet size limit:   file hdr: 65535 bytes
Number of packets:   9,698
File size:           7,642 kB
Data size:           7,487 kB
Capture duration:    1221.525013 seconds
First packet time:   2020-02-21 01:53:50.628828
Last packet time:    2020-02-21 02:14:12.153841
Data byte rate:      6,129 bytes/s
Data bit rate:       49 kbps
Average packet size: 772.03 bytes
Average packet rate: 7 packets/s
SHA256:              8b984eca8fb96799a9ad7ec5ee766937e640dc1afcad77101e5aeb0ba6be137d
RIPEMD160:           f81733fdf60d6942ebc95e76aa4c95aeb78b7115
SHA1:                8cc4f935383431e4264e482cce03fec0d4b369bd
Strict time order:   True
Number of interfaces in file: 1
Interface #0 info:
                     Encapsulation = Ethernet (1 - ether)
                     Capture length = 65535
                     Time precision = microseconds (6)
                     Time ticks per second = 1000000
                     Number of stat entries = 0
                     Number of packets = 9698

```

**Tip**: Wireshark have **Expert Information**. At bottom on left side in wireshark window is button, which open another window and this window contains any anomalies and other items which are interest in a capture file. We can use this information like hint to get picture about traffic in this pcap file and in the end write this information to report. Another way how to open windows with Expert Information is in Menu of wireshark is **Analyze -> Expert Information**. Or we can get from terminal using **tshark** with options `-z expert -q`. Options `-nr` for reading pcap file and disable all name resolutions.

Output from tshark is under name **01-pcap-tshark-expert-info.txt** in evidence folder.

Detailed information about protocols in this traffic file is **Statistics** options from Wireshark menu. In Wireshark menu open **Statistics -> Protocol Hierarchy Statistics** window.

![](./screenshot/00-pcap-wireshark-proto-hier-statistics-1.png)

For tshark use options `-qz io,phs`. For this pcap file:
`tshark -nr 2020-02-21-traffic-analysis-exercise.pcap -qz io,phs`

Output from tshark is saved in file **02-pcap-wireshark-proto-hier-statistics-1.txt**.

Another view is **Statistics -> Conversations**.

![](./screenshot/01-pcap-wireshark-conv-tcp-1.png)

You can see that highlighted row is conversation between IP address `172.17.8.174` and `205.185.216.42`. There was trasferred 2,453 kB.

Let's get information about network 172.17.8.0/24. Using tshark with options `-qz endpoints,ip` and grep with parameters `-E '^172.17.8.[0-9]{1,3}'` for filter only IP addresses from network 172.17.8.0/24. Output is saved in file **03-pcap-tshark-endpoints-1.txt**.

```
172.17.8.174            9697       7486769       3170          525544        6527         6961225
172.17.8.8              1357        358197        632          174101         725          184096
172.17.8.1                39          4290          0               0          39            4290
172.17.8.255              15          2049          0               0          15            2049
```

When we use filter in Wireshark `ip.addr==172.17.8.1`, there is only protocol NBNS (NetBIOS Name Service).

![](./screenshot/02-pcap-wireshark-proto-hier-1.png)

For IP address `172.17.8.8` is a lot of used protocols like NTP, DNS, DHCP, SAMBA, LDAP, ...

![](./screenshot/03-pcap-wireshark-proto-hier-2.png)

IP address `172.17.8.174` use a lot of protocols too (NTP, DNS, SAMBA, RPC, ...).

![](./screenshot/04-pcap-wireshark-proto-hier-3.png)

Next, use filter in wireshark `bootp`, which display DHCP traffic.

![](./screenshot/05-pcap-wireshark-filter-dhcp-1.png)

Or filter with selected fields for tshark `-Y 'bootp and bootp.option.dhcp == 5' -T fields -e bootp.ip.your -e bootp.option.dhcp_server_id -e bootp.option.router -e bootp.option.domain_name_server`. Output from thsark is saved in file **04-pcap-tshark-dhcp-1.txt**.

For now, we can say that:

```
DHCP Client: 172.17.8.174
DHCP Server: 172.17.8.8
DNS Server: 172.17.8.1
Router: 172.17.8.1
Broadcast: 172.17.8.255
```

We have basic information about IP addresses in LAN network. This capture contains NBNS traffic and this protocol can indetify hostnames for computers (Microsoft Windows or Apple MacOS). For Windows User Account is protocol Kerberos.

First, use Wireshark filter is `nbns`.

![](./screenshot/06-pcap-wireshark-filter-nbns-1.png)

For tshark use filter with selected fields `-Y 'nbns and nbns.flags.opcode == 5' -T fields -e eth.src -e nbns.name -e nbns.addr`. Output is in file **05-pcap-tshark-nbns-1.txt**.

If there is information about windows user account use filter `kerberos.CNameString`. After this filter find in Packet Detail window field `CNameString` and apply as column.

![](./screenshot/07-pcap-wireshark-filter-kerberos-1.png)

Tshark filter with selected fields `-Y 'kerberos.CNameString and ip.src == 172.17.8.174' -T fields -e ip.src -e kerberos.CNameString`. Output file **06-pcap-tshark-kerberos-1.txt**.

Information about client:

```
MAC address: 00:11:75:8c:fd:47
Hostname: DESKTOP-TZMKHKC
User account: gabriella.ventura
```

Usualy if I want to get OS name and version just look for client User Agent. For this client and this capture file are only this User Agents:

- Microsoft-Delivery-Optimization/10.0
- Mozilla/4.0 (compatible; Win32; WinHttp.WinHttpRequest.5)

Tshark filter `-Y 'ip.src == 172.17.8.174 and http.request' -T fields -e http.user_agent`.

From this user agents I cannot say which OS name and version client uses. So if in this capture file is **Server Message Block Protocol** (SMB) try this filter for Wireshark `smb.cmd and browser`. If you have some packet with this protocol, look for fields `browser.windows_version` and `browser.os_major`. And now we have a proof that client use Windows 10.

![](./screenshot/08-pcap-wireshark-filter-smb-1.png)

Tshark filter `-Y 'smb.cmd and browser' -T fields -e browser.server -e browser.os_major`.

In Protocol Hierarchy statistics we saw that, most traffic in this capture file is **Secure Socket Layer** and **Hypertext Transfer Protocol**. Let's look at Hypertext Transfer Protocol.

![](./screenshot/09-pcap-wireshark-filter-http-1.png)

Tshark filter `-Y 'ip.src == 172.17.8.174 and http.request' -T fields -e ip.src -e ip.dst -e http.host -e http.request.method -e http.host -e http.request.uri`. Output file **07-pcap-tshark-http-request-1.txt**.

In screenshot is highlighted interest lines. Client requested for file with extension **.bin** using UserAgent **Mozilla/4.0 (compatible; Win32; WinHttp.WinHttpRequest.5)** from **blueflag.xyz**. Next step is extract this file and get some hashes.

**Tip**: Right click on this line and select **Folloe -> TCP Stream**. At bottom in window **Follow TCP Stream** select conversation between `49.51.172.56` and `172.17.8.174`. After that select **Raw** in **Show and save data as** and save this frame as `yrkbdmt.bin`.

![](./screenshot/10-pcap-wireshark-filter-http-2.png)

So now we have to remove HTTP header from this file.

**Before**:

![](./screenshot/11-file-edit-http-header-1.png)

**After**:

![](./screenshot/12-file-edit-http-header-2.png)

Hash of yrkbdmt.bin:
MD5	d3575be02b074f31f21efcb1b20f0926

This hash we use in virustotal.com. Link: `https://www.virustotal.com/gui/file/6c6cdcd79fcf9beadd75f51b163785e36ddbe5b56e9276a71841429628822908/detection`

![](./screenshot/13-file-virustotal-md5-1.png)

File details:

| Basic Information |    |
| :- | :- |
| MD5 | d3575be02b074f31f21efcb1b20f0926 |
| SHA-1 | ffe34883e838e32dd861cc78e7cd8f92ae889a02 |
| SHA-256 | 6c6cdcd79fcf9beadd75f51b163785e36ddbe5b56e9276a71841429628822908 |
| Vhash | 025076567d751d75151035z31zd9z17z21zbfz |
| Authentihash | 6787172bb8c27b4c563620a8a3696b6520c6b2a2b64829b2817e7bae22529a2c |
| Imphash | b54271bcaf179ca994623a6051fbc2ba |
| SSDEEP | 6144:vDwYweNHD22Pw2VcYDyw0pkBn88oXhp97x:v9LH5YQcYDNakBmhp97x |
| File type | Win32 EXE |
| Magic | PE32 executable for MS Windows (console) Intel 80386 32-bit |
| File size | 204.00 KB (208897 bytes) |

Now, try to search requested uri (`/nCvQOQHCBjZFfiJvyVGA/yrkbdmt.bin`) in google. You can find a lot of links where this uri is used **Dridex is a banking Trojan**.

If we want IP address for host `blueflag.xyz`, use filter `dns.resp.name == "blueflag.xyz"`.

![](./screenshot/14-pcap-wireshark-filter-host-1.png)

For Tshark you can use this filter `-Y 'dns.resp.name == "blueflag.xyz"' -T fields -e ip.src -e ip.dst -e dns.resp.name -e dns.a`.

In alert log from Suricata is same proof like our analysis.

![](./screenshot/15-alerts-splunk-table-events-1.png)

At this point we can write this informations for report:

| Infection details |    |
| :- | :- |
| Date and time | 21.02.2020 00:55 UTC (01:55 CET) |
| Client MAC Address | 00:11:75:8c:fd:47 |
| Client IP Address | 172.17.8.174 |
| Client Hostname | DESKTOP-TZMKHKC |
| Client OS  | Windows 10 |
| Client Windows account | gabriella.ventura |
| IP Address of infected host | 49.51.172.56 |
| Infection host | blueflag.xyz |
| Infection URL | /nCvQOQHCBjZFfiJvyVGA/yrkbdmt.bin |

There is a little bit more, but now it's enough. :)
