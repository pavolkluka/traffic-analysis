
===================================================================
Protocol Hierarchy Statistics
Filter: 

eth                                      frames:9698 bytes:7487150
  ip                                     frames:9698 bytes:7487150
    udp                                  frames:227 bytes:33615
      bootp                              frames:2 bytes:723
      mdns                               frames:4 bytes:344
      llmnr                              frames:2 bytes:150
      dns                                frames:129 bytes:17177
      cldap                              frames:32 bytes:8234
      nbns                               frames:51 bytes:5610
      nbdgm                              frames:3 bytes:729
        smb                              frames:3 bytes:729
          mailslot                       frames:3 bytes:729
            browser                      frames:3 bytes:729
      ntp                                frames:4 bytes:648
    igmp                                 frames:10 bytes:548
    tcp                                  frames:9461 bytes:7452987
      ldap                               frames:155 bytes:56247
        tcp.segments                     frames:31 bytes:24963
          ldap                           frames:10 bytes:14050
      nbss                               frames:251 bytes:61699
        smb                              frames:4 bytes:508
        smb2                             frames:245 bytes:61081
          tcp.segments                   frames:4 bytes:2406
          data                           frames:4 bytes:3034
            tcp.segments                 frames:1 bytes:1478
          smb2                           frames:16 bytes:6049
            smb2                         frames:6 bytes:2148
          dcerpc                         frames:32 bytes:8774
            samr                         frames:30 bytes:8190
      dcerpc                             frames:164 bytes:48340
        epm                              frames:28 bytes:7616
        rpc_netlogon                     frames:8 bytes:3380
        tcp.segments                     frames:8 bytes:5292
        drsuapi                          frames:76 bytes:21880
        lsarpc                           frames:2 bytes:716
      kerberos                           frames:46 bytes:12548
        tcp.segments                     frames:30 bytes:7560
      ssl                                frames:764 bytes:583019
        tcp.segments                     frames:297 bytes:337806
          ssl                            frames:214 bytes:283463
      data                               frames:20 bytes:1100
      http                               frames:56 bytes:30338
        data                             frames:21 bytes:15524
          tcp.segments                   frames:8 bytes:7286
===================================================================
